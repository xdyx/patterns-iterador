package test;

import cre.CreApi;
import cre.CreUser;
import cre.CreUserAggregate;
import iterator.Aggregate;
import iterator.Iterator;


public class IteratorApp {

    public static void main(String[] args) {

        Aggregate creUserAggregate = new CreUserAggregate();
        printItemsFrom(creUserAggregate.createIterator());

    }

    public static void printItemsFrom(Iterator iterator) {
        while (!iterator.isDone()) {                  //itrator Object.
            System.out.println(iterator.currentObj());
        }
    }
}
