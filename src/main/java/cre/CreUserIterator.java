package cre;

import iterator.Iterator;

import java.util.List;

public class CreUserIterator implements Iterator {


    private java.util.Map<Integer, UserData> creUsersArray;
    private java.util.Iterator<java.util.Map.Entry<Integer, UserData>> iterator;

    public CreUserIterator() {

        this.creUsersArray = CreApi.getCREinfo();
        iterator = creUsersArray.entrySet().iterator();
    }


    public boolean isDone() {

        return !iterator.hasNext();

    }


    public String currentItem() {
        return iterator.next().toString();
    }


    public java.util.Map.Entry<Integer, UserData> currentObj() {
        java.util.Map.Entry<Integer, UserData> entry = iterator.next();
        return entry;
    }
}
