package cre;

public class CreUser {
    private int id;
    private UserData userData;

    public CreUser(int id, UserData userData) {
        this.id = id;
        this.userData = userData;
    }

    public int getID() {
        return id;
    }

    public UserData getUserData() {
        return userData;
    }


    @Override
    public String toString() {
        return String.format("CREUserId: %s UserData: %s", this.id, this.userData);
    }


}
