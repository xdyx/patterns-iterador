package cre;

import java.util.Arrays;
import java.util.List;

public class CreApi {

    public static List<CreUser> getCreUsers() {
        List<CreUser> users =
                Arrays.asList(
                        new CreUser(1, new UserData("login", "direccion", 22.58)),
                        new CreUser(2, new UserData("login2", "direccion2", 45.45)),
                        new CreUser(3, new UserData("login3", "direccion3", 444.58)));
        return users;
    }

    public static java.util.Map<Integer, UserData> getCREinfo() {
        java.util.Map<Integer, UserData> users = new java.util.HashMap<Integer, UserData>();
        for (CreUser cre : getCreUsers()) {
            users.put(cre.getID(), cre.getUserData());
        }
        return users;
    }
}
