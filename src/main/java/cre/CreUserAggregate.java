package cre;

import iterator.Aggregate;
import iterator.Iterator;

public class CreUserAggregate implements Aggregate {

    public Iterator createIterator() {
        return new CreUserIterator();
    }
}
