package iterator;

public interface Iterator {

    boolean isDone();

    String currentItem();

    Object currentObj();
}
