# [TAREA GRUPAL 06]: Cliente CRE. Patron Iterador #

### Grupo 1 ###

### Integrantes ###

* GIL MONTERO MARCO ANTONIO
* VERDI TORREZ FRANZ AJIT SINGH
* AILLON SALINAS RAUL FERNANDO
* PARRAGA MARTINEZ ANA ISABEL

###Instrucciones ###
crear la clase UserData
crear la clase CreUserIterator

crear la interfaz Iterator
crear la interfaz Aggregate
crear la clase concrete Iterator CreUserIterator
crear la clase concreteaggregate CreUserAggregate

1. Crear la interfaz Iterator, donde se definirán las operaciones para el recorrido de elementos 
2. Crear la interfaz Aggregate, donde se definirá la operación para crear Iterators	
3. Crear la clase Concrete iterator que implementara la interfaz Iterator CreUserIterator, tomando en cuenta 
 que esta debe guardar la posición actual, además debe acceder
 a la colección java.util.Map<Integer, UserData> getCREinfo() implementada en CreAPI
 que se quiere recorrer.	
4 Crear la clase ConcreteAggregate que implementa la interfaz CreUserAggregate, esta debe 
retornar un Iterator concreto para su estructura de datos.	
 
 